pipeline {
	agent any

	parameters {
		booleanParam(name: 'UNRESTRICTED', defaultValue: false, description: 'Enable this to allow to publish a build that still have some minor issue (unstable)')
	}

	post {
		always {
			recordIssues enabledForFailure: true, tool: groovyScript(parserId: 'rustc_warnings'), qualityGates: [[threshold: 1, type: 'TOTAL', unstable: true]]
			recordIssues enabledForFailure: true, tool: groovyScript(parserId: 'clippy_warnings'), qualityGates: [[threshold: 1, type: 'TOTAL_NORMAL', unstable: true], [threshold: 1, type: 'TOTAL_ERROR', unstable: false]]
			archiveArtifacts artifacts: 'report/*', fingerprint: false
			archiveArtifacts artifacts: 'target/doc/*', fingerprint: false
			archiveArtifacts artifacts: 'target/i686-unknown-linux-gnu/doc/*', fingerprint: false
			junit 'junitify_report.xml'
		}
	}

	stages {
		stage('setup') {
			steps {
				sh "cargo clean"
			}
		}

		stage('Build') {
			steps {
				sh "cargo build --verbose"
			}
		}

		stage('Test') {
			steps {
				sh "cargo test -- -Z unstable-options --format json --report-time | junitify > junitify_report.xml"
				sh '''
				sed -i '/<test>/d' junitify_report.xml
				sed -i '/<\\/test>/d' junitify_report.xml
				sed -i '1!{/<?xml version="1.0" encoding="UTF-8"?>/d}' junitify_report.xml
				sed -i 's/<?xml version="1.0" encoding="UTF-8"?>/<?xml version="1.0" encoding="UTF-8"?><testsuites>/g' junitify_report.xml
				echo "</testsuites>" >> junitify_report.xml
				'''
			}
		}

		stage('Check') {
			parallel {

				stage('Clippy') {
					steps {
						script {
							try {
								sh "cargo clippy --all"
							} catch (e) {
								unstable("Clippy signal some errors")
							}
						}
					}
				}

				stage('Rustfmt') {
					steps {
						script {
							try {
								sh "cargo fmt --all --check --"
							} catch (e) {
								unstable("Rustfmt recommand some actions")
							}
						}
					}
				}

				stage('MegaLinter') {
					agent {
						docker {
							image 'megalinter/megalinter-rust:v5'
							args "-u root -e VALIDATE_ALL_CODEBASE=true -v ${WORKSPACE}:/tmp/lint --entrypoint=''"
							reuseNode true
						}
					}
					steps {
						sh '/entrypoint.sh'
					}
				}
			}
		}

		stage('Doc') {
			steps {
				sh "cargo doc"
			}
		}

		stage('Publish') {
			when {
				expression {
					currentBuild.result == null || currentBuild.result == 'SUCCESS' || params.UNRESTRICTED == true
				}
			}
			steps {
				sh "cd substance-macro && cargo publish --dry-run"
				sh "cd substance-runner && cargo publish --dry-run"
				withCredentials([string(credentialsId: 'CARGO_REGISTRY_TOKEN', variable: 'CARGO_REGISTRY_TOKEN')]) {
					sh "cd substance-macro && cargo publish"
					sh "cd substance-runner && cargo publish"
				}
			}
		}
	}
}
