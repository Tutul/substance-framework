
<a name="0.3.0-alpha"></a>
## [0.3.0-alpha](https://gitlab.com/Tutul/substance-framework/compare/0.2.0-alpha...0.3.0-alpha)

> 2022-04-27

### Devops

* Try to resolve groovy and shell elements (final_v2)
* Try to resolve groovy and shell elements (final)
* Try to resolve groovy and shell elements (round 4...)
* Try to resolve groovy and shell elements (round 3)
* Try to resolve groovy and shell elements (round 2)
* Try to resolve groovy and shell elements
* Fix some Junity error
* Enable JUnit report using junitify
* Disable artifacts saving for the rlib
* Enable testing for Jenkins

### Feat

* clean before new release
* Use Trybuild for UI testing of the proc-macro part

### Wip

* working on unit test for the runner


<a name="0.2.0-alpha"></a>
## [0.2.0-alpha](https://gitlab.com/Tutul/substance-framework/compare/0.1.1-alpha...0.2.0-alpha)

> 2022-04-16

