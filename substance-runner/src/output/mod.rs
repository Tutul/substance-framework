/***********************************************************************************
 * MIT License                                                                     *
 *                                                                                 *
 * Copyright (c) 2022 Tutul                                                        *
 *                                                                                 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy    *
 * of this software and associated documentation files (the "Software"), to deal   *
 * in the Software without restriction, including without limitation the rights    *
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell       *
 * copies of the Software, and to permit persons to whom the Software is           *
 * furnished to do so, subject to the following conditions:                        *
 *                                                                                 *
 * The above copyright notice and this permission notice shall be included in all  *
 * copies or substantial portions of the Software.                                 *
 *                                                                                 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *
 * SOFTWARE.                                                                       *
 ***********************************************************************************/

use crate::{Conclusion, Outcome, TestBlock};

pub(crate) mod combined;
pub(crate) mod pretty;
pub(crate) mod report;

pub trait Output {
    fn new() -> Self
    where
        Self: Sized;
    fn init(&mut self, tests: &[&TestBlock]);

    fn no_test(&self);

    fn print_title(&self);
    fn print_test(&self, test_name: &str);
    fn print_single_outcome(&self, test_outcome: &Outcome);
    fn print_summary(&self, conclusion: &Conclusion);
}

#[cfg(test)]
pub(crate) mod dummy {
    use super::*;

    pub struct Dummy {}
    impl Output for Dummy {
        fn new() -> Self
        where
            Self: Sized,
        {
            Self {}
        }
        fn init(&mut self, _tests: &[&TestBlock]) {}
        fn no_test(&self) {}
        fn print_title(&self) {}
        fn print_test(&self, _test_name: &str) {}
        fn print_single_outcome(&self, _test_outcome: &Outcome) {}
        fn print_summary(&self, _conclusion: &Conclusion) {}
    }
}
