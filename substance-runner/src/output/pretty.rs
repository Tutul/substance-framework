/***********************************************************************************
 * MIT License                                                                     *
 *                                                                                 *
 * Copyright (c) 2022 Tutul                                                        *
 *                                                                                 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy    *
 * of this software and associated documentation files (the "Software"), to deal   *
 * in the Software without restriction, including without limitation the rights    *
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell       *
 * copies of the Software, and to permit persons to whom the Software is           *
 * furnished to do so, subject to the following conditions:                        *
 *                                                                                 *
 * The above copyright notice and this permission notice shall be included in all  *
 * copies or substantial portions of the Software.                                 *
 *                                                                                 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *
 * SOFTWARE.                                                                       *
 ***********************************************************************************/

use crate::output::*;
use crate::{Conclusion, Outcome, TestBlock};

// ANSI original color code for the foreground
#[allow(dead_code)]
enum AnsiColorFg {
    Black = 30,
    Red = 31,
    Green = 32,
    Yellow = 33,
    Blue = 34,
    Magenta = 35,
    Cyan = 36,
    White = 37,
    BrightBlack = 90,
    BrightRed = 91,
    BrightGreen = 92,
    BrightYellow = 93,
    BrightBlue = 94,
    BrightMagenta = 95,
    BrightCyan = 96,
    BrightWhite = 97,
}

// ANSI original color code for the background
#[allow(dead_code)]
enum AnsiColorBg {
    Black = 40,
    Red = 41,
    Green = 42,
    Yellow = 43,
    Blue = 44,
    Magenta = 45,
    Cyan = 46,
    White = 47,
    BrightBlack = 100,
    BrightRed = 101,
    BrightGreen = 102,
    BrightYellow = 103,
    BrightBlue = 104,
    BrightMagenta = 105,
    BrightCyan = 106,
    BrightWhite = 107,
}

// ANSI original control code
#[allow(dead_code)]
enum AnsiAttr {
    Reset,
    Bold,
    Underline,
}

pub struct Pretty {
    name_width: usize,
    plural: bool,
    total_tests: usize,
}

impl Output for Pretty {
    fn new() -> Self {
        Self {
            name_width: 0,
            plural: false,
            total_tests: 0,
        }
    }

    fn init(&mut self, tests: &[&TestBlock]) {
        /* Determine max test name length for the formatting later
         *
         * Unicode is hard and there is no way we can properly align/pad the
         * test names and outcomes. But most of the tests name should be in
         * ASCII, so it will be probably ok
         */
        let name_width = tests
            .iter()
            .map(|test| test.name.chars().count())
            .max()
            .unwrap_or(0);

        self.name_width = name_width;
        self.total_tests = tests.len();
        self.plural = self.total_tests > 1;
    }

    fn no_test(&self) {
        libc_println!();
        libc_print!(
            "\u{001b}[{};{}",
            AnsiAttr::Underline as i32,
            AnsiColorFg::BrightBlue as i32
        );
        libc_print!("No test to run!");
        libc_println!("\u{001b}[00m");
        libc_println!();
    }

    fn print_title(&self) {
        libc_println!();
        libc_println!(
            "running \u{001b}[{}m{}\u{001b}[00m test{}",
            AnsiColorFg::Blue as i32,
            self.total_tests,
            if self.plural { "s" } else { "" }
        );
    }

    fn print_test(&self, name: &str) {
        libc_print!(
            "test \u{001b}[{}m{: <2$}\u{001b}[00m ... ",
            AnsiColorFg::Magenta as i32,
            name,
            self.name_width
        );
    }

    fn print_single_outcome(&self, outcome: &Outcome) {
        libc_print!("\u{001b}[");
        match outcome {
            Outcome::Ignored => libc_print!("{}mignored", AnsiColorFg::Yellow as i32),
            Outcome::Passed => libc_print!("{}m   PASS", AnsiColorFg::BrightGreen as i32),
            Outcome::Failed { .. } => libc_print!(
                "{};{}m   FAIL",
                AnsiAttr::Bold as i32,
                AnsiColorFg::Red as i32
            ),
            Outcome::Aborted { .. } => libc_print!(
                "{};{};{}m  ABORT",
                AnsiAttr::Bold as i32,
                AnsiColorFg::BrightWhite as i32,
                AnsiColorBg::Red as i32
            ),
            Outcome::Skipped => libc_print!(
                "{};{}m   skip",
                AnsiAttr::Underline as i32,
                AnsiColorFg::BrightBlack as i32
            ),
        };
        libc_println!("\u{001b}[00m");
        match outcome {
            Outcome::Failed { reason } => {
                if reason.is_some() {
                    libc_println!("   > {}", reason.unwrap())
                }
            }
            Outcome::Aborted { reason } => {
                if reason.is_some() {
                    libc_println!("!>>> {}", reason.unwrap())
                }
            }
            _ => {}
        }
    }

    fn print_summary(&self, conclusion: &Conclusion) {
        libc_println!();
        libc_print!("test result: \u{001b}[{};", AnsiAttr::Bold as i32);

        if conclusion.has_aborted {
            libc_print!(
                "{};{}mABORTED",
                AnsiColorFg::BrightWhite as i32,
                AnsiColorBg::Red as i32
            );
        } else if conclusion.num_failed > 0 {
            libc_print!("{}mFAILED", AnsiColorFg::Red as i32);
        } else {
            libc_print!("{}mPASSED", AnsiColorFg::BrightGreen as i32);
        }

        libc_println!("\u{001b}[00m");

        libc_print!("\u{001b}[{}m{}\u{001b}[00m test{} run; \u{001b}[{}m{}\u{001b}[00m passed; \u{001b}[{}m{}\u{001b}[00m failed; \u{001b}[{}m{}\u{001b}[00m ignored",
            AnsiColorFg::Blue as i32, conclusion.num_passed + conclusion.num_failed + conclusion.num_ignored,
            if self.plural {
                "s"
            } else {
                ""
            },
            AnsiColorFg::BrightGreen as i32, conclusion.num_passed,
            AnsiColorFg::Red as i32, conclusion.num_failed,
            AnsiColorFg::Yellow as i32, conclusion.num_ignored);

        if conclusion.has_aborted {
            libc_println!(
                "; \u{001b}[{}m{}\u{001b}[00m skipped",
                AnsiColorFg::BrightBlack as i32,
                conclusion.num_skipped
            );
        } else {
            libc_println!();
        }

        libc_println!();
    }
}
