/***********************************************************************************
 * MIT License                                                                     *
 *                                                                                 *
 * Copyright (c) 2022 Tutul                                                        *
 *                                                                                 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy    *
 * of this software and associated documentation files (the "Software"), to deal   *
 * in the Software without restriction, including without limitation the rights    *
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell       *
 * copies of the Software, and to permit persons to whom the Software is           *
 * furnished to do so, subject to the following conditions:                        *
 *                                                                                 *
 * The above copyright notice and this permission notice shall be included in all  *
 * copies or substantial portions of the Software.                                 *
 *                                                                                 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   *
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   *
 * SOFTWARE.                                                                       *
 ***********************************************************************************/

/// Provide a panic-like macro that can be used in the framework to detect a test failure and save
/// that status with a message for later screen printing.
///
/// std unit tests rely heavily on unwinding. Each unit test is run inside a catch_unwind block.
/// If the unit test panics then the panic is caught and the test is marked as 'failed'
/// (or as 'passed' if the unit test was marked with `#[should_panic]`).
///
/// The framework attempts to emulate this behavior by by-passing the panic macro to mark the test
/// failed and then early return instead of unwind. Of course, this emulation doesn't work on panic!
/// originates from outside the crate under test, because panic is not overridden in that scope.
/// This macro provides a way to differentiate between true panic and test failure one.
///
/// This emulation has some limitations. For instance, it can only causes one panicking function to
/// return. To remediate on that, any subsequent panic! call will directly return causing some panic
/// to be invisible until they get the chance to be the first one. To provide a bit of insight, the
/// macro also sets a `panicking` flag to let you know that another panic was thrown after the first
/// one (but without any messages or context).
/// If a panicking panic is triggered again, the framework will consider either the tests are ill written
/// or the failure is too fatal. In this case real panic will cause an abort!
#[macro_export]
macro_rules! substance_panic {
    () => (
        substance_panic!("explicit panic")
    );
    ($fmt:expr) => ({
        substance_panic!($fmt,)
    });
    ($fmt:expr, $($arg:tt)*) => ({
        #[allow(improper_ctypes)]
        {
            let mut state = substance_framework::STATE.write();
            if state.panic {
                substance_framework::crash(file!(), line!(), column!(), Some(&format_args!("{}", "panic is panicking again")));
            } else {
                state.panic = true;
                state.msg = format_args!($fmt, $($arg)*).as_str();
                return
            }
        }
    });
}

//////////////////////////////////////////////

/// Bypass the default panic macro and redirect all its calls to our custom implementation.
/// This is necessary to prevent unwinding and let us detect test failure with some assertions.
///
/// cfr. [substance_panic!]
#[macro_export]
macro_rules! panic {
    ($($tt:tt)*) => {
        substance_panic!($($tt)*);
    };
}

//////////////////////////////////////////////

/// Passes if Condition is true.
///
/// Evaluates the condition and passes if it is true.
/// Otherwise, the test is marked as failure.
/// The optional string is printed on failure.
///
/// # Examples
///
/// ```rust
/// let my_variable = 37;
/// sf_assert!(my_variable == 37);
/// ```
///
/// ```rust
/// fn my_function() -> bool {
///     return false;
/// }
///
/// sf_assert!(my_function(), "this test is a failure");
/// ```
#[macro_export]
macro_rules! sf_assert {
    ($cond:expr $(,)?) => ({
        match (&$cond) {
            cond_bool => {
                sf_assert!($cond, "assertion failed: {} is true", &*cond_bool);
            }
        }
    });
    ($cond:expr, $($arg:tt)+) => ({
        match (&$cond) {
            cond_bool => {
                if ! *cond_bool {
                    substance_panic!("{}", format_args!($($arg)+));
                }
            }
        }
    });
}

//////////////////////////////////////////////

/// Passes if both arguments are equals.
///
/// Passes if the left argument is equal to the right argument.
/// Otherwise, the test is marked as failure.
///
/// # Examples
///
/// ```rust
/// let left = 37;
/// let right = 37;
/// sf_assert_eq!(left, right);
/// ```
///
/// ```rust
/// sf_assert_eq!(0, 5, "this test is a failure");
/// ```
#[macro_export]
macro_rules! sf_assert_eq {
    ($left:expr, $right:expr $(,)?) => ({
        match (&$left, &$right) {
            (left_val, right_val) => {
                sf_assert_eq!($left, $right, "assertion failed: {} == {}", &*left_val, &*right_val);
            }
        }
    });
    ($left:expr, $right:expr, $($arg:tt)+) => ({
        match (&$left, &$right) {
            (left_val, right_val) => {
                if !(*left_val == *right_val) {
                    substance_panic!("{}", format_args!($($arg)+));
                }
            }
        }
    });
}

//////////////////////////////////////////////

/// Passes if both arguments are not equal.
///
/// Passes if the left argument is not equal to the right argument.
/// Otherwise, the test is marked as failure.
///
/// # Examples
///
/// ```rust
/// let left = 37;
/// let right = 42;
/// sf_assert_ne!(left, right);
/// ```
///
/// ```rust
/// sf_assert_ne!(5, 5, "this test is a failure");
/// ```
#[macro_export]
macro_rules! sf_assert_ne {
    ($left:expr, $right:expr $(,)?) => ({
        match (&$left, &$right) {
            (left_val, right_val) => {
                sf_assert_ne!($left, $right, "assertion failed: {} != {}", &*left_val, &*right_val);
            }
        }
    });
    ($left:expr, $right:expr, $($arg:tt)+) => ({
        match (&($left), &($right)) {
            (left_val, right_val) => {
                if !(*left_val != *right_val) {
                    substance_panic!("{}", format_args!($($arg)+));
                }
            }
        }
    });
}

//////////////////////////////////////////////

/// Passes if the left argument is strictly smaller than the right one.
///
/// Passes if the left argument is strictly smaller than the right argument.
/// Otherwise, the test is marked as failure.
///
/// # Examples
///
/// ```rust
/// let left = -325;
/// let right = 37;
/// sf_assert_lt!(left, right);
/// ```
///
/// ```rust
/// sf_assert_lt!(0, -3, "this test is a failure");
/// ```
#[macro_export]
macro_rules! sf_assert_lt {
    ($left:expr, $right:expr $(,)?) => ({
        match (&$left, &$right) {
            (left_val, right_val) => {
                sf_assert_lt!($left, $right, "assertion failed: {} < {}", &*left_val, &*right_val);
            }
        }
    });
    ($left:expr, $right:expr, $($arg:tt)+) => ({
        match (&($left), &($right)) {
            (left_val, right_val) => {
                if !(*left_val < *right_val) {
                    substance_panic!("{}", format_args!($($arg)+));
                }
            }
        }
    });
}

//////////////////////////////////////////////

/// Passes if the left argument is strictly smaller than or equal to the right one.
///
/// Passes if the left argument is strictly smaller than or equal to the right argument.
/// Otherwise, the test is marked as failure.
///
/// # Examples
///
/// ```rust
/// let left = -325;
/// let right = 37;
/// sf_assert_le!(left, right);
/// ```
///
/// ```rust
/// sf_assert_le!(0, 0);
/// ```
///
/// ```rust
/// sf_assert_le!(0, -3, "this test is a failure");
/// ```
#[macro_export]
macro_rules! sf_assert_le {
    ($left:expr, $right:expr $(,)?) => ({
        match (&$left, &$right) {
            (left_val, right_val) => {
                sf_assert_le!($left, $right, "assertion failed: {} <= {}", &*left_val, &*right_val);
            }
        }
    });
    ($left:expr, $right:expr, $($arg:tt)+) => ({
        match (&($left), &($right)) {
            (left_val, right_val) => {
                if !(*left_val <= *right_val) {
                    substance_panic!("{}", format_args!($($arg)+));
                }
            }
        }
    });
}

//////////////////////////////////////////////

/// Passes if the left argument is strictly greater than the right one.
///
/// Passes if the left argument is strictly greater than the right argument.
/// Otherwise, the test is marked as failure.
///
/// # Examples
///
/// ```rust
/// let left = 325;
/// let right = 37;
/// sf_assert_gt!(left, right);
/// ```
///
/// ```rust
/// sf_assert_gt!(-6, 32, "this test is a failure");
/// ```
#[macro_export]
macro_rules! sf_assert_gt {
    ($left:expr, $right:expr $(,)?) => ({
        match (&$left, &$right) {
            (left_val, right_val) => {
                sf_assert_gt!($left, $right, "assertion failed: {} > {}", &*left_val, &*right_val);
            }
        }
    });
    ($left:expr, $right:expr, $($arg:tt)+) => ({
        match (&($left), &($right)) {
            (left_val, right_val) => {
                if !(*left_val > *right_val) {
                    substance_panic!("{}", format_args!($($arg)+));
                }
            }
        }
    });
}

//////////////////////////////////////////////

/// Passes if the left argument is strictly greater than or equals to the right one.
///
/// Passes if the left argument is strictly greater than or equals to the right argument.
/// Otherwise, the test is marked as failure.
///
/// # Examples
///
/// ```rust
/// let left = 325;
/// let right = 37;
/// sf_assert_ge!(left, right);
/// ```
///
/// ```rust
/// sf_assert_ge!(435, 435);
/// ```
///
/// ```rust
/// sf_assert_ge!(45, 645884, "this test is a failure");
/// ```
#[macro_export]
macro_rules! sf_assert_ge {
    ($left:expr, $right:expr $(,)?) => ({
        match (&$left, &$right) {
            (left_val, right_val) => {
                sf_assert_ge!($left, $right, "assertion failed: {} >= {}", &*left_val, &*right_val);
            }
        }
    });
    ($left:expr, $right:expr, $($arg:tt)+) => ({
        match (&($left), &($right)) {
            (left_val, right_val) => {
                if !(*left_val >= *right_val) {
                    substance_panic!("{}", format_args!($($arg)+));
                }
            }
        }
    });
}

//////////////////////////////////////////////

/// Passes if the argument is an Ok result.
///
/// Passes if the argument is an instance of Result::Ok(T).
/// Otherwise, the test is marked as failure.
///
/// # Examples
///
/// ```rust
/// sf_assert_ok!(my_function_ok());
/// ```
///
/// ```rust
/// let empty = Result::Ok();
/// sf_assert_ok!(empty);
/// ```
///
/// ```rust
/// let err = Result::Err();
/// sf_assert_ok!(err, "this test is a failure");
/// ```
#[macro_export]
macro_rules! sf_assert_ok {
    ($cond:expr $(,)?) => ({
        match (&$cond) {
            cond_val => {
                sf_assert_ok!($cond, "assertion failed: is Ok");
            }
        }
    });
    ($cond:expr, $($arg:tt)+) => ({
        match (&$cond) {
            cond_val => {
                if ! cond_val.is_ok() {
                    substance_panic!("{}", format_args!($($arg)+));
                }
            }
        }
    });
}

//////////////////////////////////////////////

/// Passes if the argument is an existing optional value.
///
/// Passes if the argument is an instance of Option::Some(T).
/// Otherwise, the test is marked as failure.
///
/// # Examples
///
/// ```rust
/// let empty = Option::Some();
/// sf_assert_some!(empty);
/// ```
///
/// ```rust
/// let err = Option::None();
/// sf_assert_some!(err, "this test is a failure");
/// ```
#[macro_export]
macro_rules! sf_assert_some {
    ($cond:expr $(,)?) => ({
        match (&$cond) {
            cond_val => {
                sf_assert_some!($cond, "assertion failed: is Some");
            }
        }
    });
    ($cond:expr, $($arg:tt)+) => ({
        match (&$cond) {
            cond_val => {
                if ! cond_val.is_some() {
                    substance_panic!("{}", format_args!($($arg)+));
                }
            }
        }
    });
}
